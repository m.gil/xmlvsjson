#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      edgfa
#
# Created:     16/01/2019
# Copyright:   (c) edgfa 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass

import xml.etree.ElementTree as ET
tree = ET.parse('gossos.xml')
root = tree.getroot()

for a in root.iter('gos'):

    nom = a.find('nom').text
    grup = a.find('grup').text
    migpes = a.find('migpes').text
    migmida = a.find('migmida').text
    print("Nom: {} \nGrup: {} \nMitjana de pes: {} kg \nMitjana de mida: {} cm.\n".format(nom,grup,migpes,migmida))


