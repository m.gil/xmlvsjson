#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      edgfa
#
# Created:     16/01/2019
# Copyright:   (c) edgfa 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass

import json
with open('gossos.json') as file:
    #data = json.load(file)
    data = json.load(open('gossos.json'))
    for gos in data["gossos"]:
        print('Nom:', gos["nom"])
        print('Grup:', gos['grup'])
        print('Mitjana pes:', gos['migpes'],'kg')
        print('Mitjana mida:', gos['migmida'],'cm')
        print('')
